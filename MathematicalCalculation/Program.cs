﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CSharp;
using System.Numerics;
using System.Runtime.InteropServices;

namespace MathematicalCalculation
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        const int POWER = 100000;
        static void Main(string[] args)
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
            BigInteger result = Factorial(POWER);
            Environment.Exit(0);
        }
        static readonly int DegreeOfParallelism = Environment.ProcessorCount;

        static BigInteger Factorial(long x)
        {
            // Make as many parallel tasks as our DOP
            // And make them operate on separate subsets of data
            var parallelTasks =
                Enumerable.Range(1, DegreeOfParallelism)
                            .Select(i => Task.Factory.StartNew(() => Multiply(x, i),
                                         TaskCreationOptions.LongRunning))
                            .ToArray();

            // after all tasks are done...
            Task.WaitAll(parallelTasks);

            // ... take the partial results and multiply them together
            BigInteger finalResult = 1;

            foreach (var partialResult in parallelTasks.Select(t => t.Result))
            {
                finalResult *= partialResult;
            }

            return finalResult;
        }
        static BigInteger Multiply(long upperBound, int startFrom)
        {
            BigInteger result = 1;

            for (var i = startFrom; i <= upperBound; i += DegreeOfParallelism)
                result *= i;

            return result;
        }
    }
}
