﻿using System;
using System.Net;
using System.Runtime.InteropServices;

namespace DownloadVideo
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        const string URL = @"https://speed.hetzner.de/1GB.bin";
        const string FILE_NAME = "file.zip";

        public static void Main(string[] args)
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
            using (var client = new WebClient())
            {
                client.DownloadFile(URL, FILE_NAME);
            }
            Environment.Exit(0);
        }
    }
}
