﻿using System;
using System.Windows;

namespace Practice_28_05
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void mathSoluteButtonClick(object sender, RoutedEventArgs e)
        {
            var mathApplication = AppDomain.CreateDomain("MathematicalCalculation");
            var mathApplicationPath = mathApplication.BaseDirectory + "MathematicalCalculation.exe";

            mathApplication.ExecuteAssembly(mathApplicationPath);
            AppDomain.Unload(mathApplication);
            MessageBox.Show("Математическая операция завершена!");
        }

        private void downloadVideoButtonClick(object sender, RoutedEventArgs e)
        {
            var downloadApplication = AppDomain.CreateDomain("DownloadFile");
            var downloadApplicationPath = downloadApplication.BaseDirectory + "DownloadFile.exe";

            downloadApplication.ExecuteAssembly(downloadApplicationPath);
            AppDomain.Unload(downloadApplication);
            MessageBox.Show("Скачивание завершено!");
        }
    }
}
